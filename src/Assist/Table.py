'''
Created on Oct 5, 2015

@author: master
'''
import Tkinter as tk


class Table(tk.Frame):
	def __init__(self, parent, rows=1, columns=1):
		# use black background so it "peeks through" to 
		# form grid lines
		tk.Frame.__init__(self, parent, background="black")
		#tk.Frame.__init__(self, parent)
		self._widgets = []
		for row in range(rows):
			current_row = []
			for column in range(columns):
				label = tk.Label(self, text="%s/%s" % (row, column), borderwidth=0, width=10)
				label.grid(row=row, column=column, sticky="nsew", padx=1, pady=1)
				current_row.append(label)
			self._widgets.append(current_row)

		for column in range(columns):
			self.grid_columnconfigure(column, weight=1)


	def set(self, row, column, value):
		widget = self._widgets[row][column]
		widget.configure(text=value)