from Model import Model
from View import View
from Input import Input
from Control import Control
from Analysis import *

def main():
	model = Model()
	view = View()
	input = Input()
	control = Control()
	analysis = Analisys()
	
	all = {}
	all["model"] = model
	all["view"]= view
	all["input"] = input
	all["control"] = control
	all["analysis"] = analysis
	
	model.getOther(all = all)
	view.getOther(all = all)
	input.getOther(all = all)
	control.getOther(all = all)
	analysis.getOther(all = all)
	
	model.update()
	view.update()
	input.update()
	control.update()
	analysis.update()

	control.image.addImage(URL = "../../Pictures/Project3/20151010_231619.jpg", side = "left")
	control.image.addImage(URL = "../../Pictures/Project3/20151010_231718.jpg", side = "right")

	#control.image.addImage(URL = "../img/Stalin3.jpeg", side = "left")
	#control.image.addImage(URL = "../img/Stalin2.jpeg", side = "left")
	#control.image.addImage(URL = "../img/Stalin.jpeg", side = "left")	
	#control.image.addImage(URL = "../img/Stalin3.jpeg", side = "left")	
	
	#control.image.addImage(URL = "../img/Gandhi.jpeg", side = "right")
	#control.image.addImage(URL = "../img/Gandhi2.jpeg", side = "right")
	#control.image.addImage(URL = "../img/Gandhi2.jpeg", side = "right")

	
	model.run()
	input.run()
	control.run()
	view.run()
	
main()