'''
Created on Oct 4, 2015

@author: master
'''
from View import Body
from View import Popup

from Tkinter import *
from Tkinter import Tk, Frame


class View(Frame):
	main = None
	
	def __init__(self, master = None):
		self.parent = master
		self.initUI()

	def initUI(self):
		self.root = Tk()
		self.root.wm_title("Project3")
		self.root.geometry("%dx%d+0+0" % (1200, 380))
				
	def getOther(self, all=None):
		self.all = all
		
	def update(self):
		self.body = Body.Body(self.root, self.all)
		self.popup = Popup.Popup(self.root,self.all)
		
	def run(self):
		self.root.mainloop()