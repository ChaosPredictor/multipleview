'''
Created on Oct 4, 2015

@author: master
'''

from Tkinter import *
from View import ImageCanvas

class FrameObject(Frame):
	
	#canvas = None
	#activeId = False
	acvite = False
	
	def __init__(self, master = None, control=None, URL = None, imageId=None, all=None):
		#Frame.__init__(self, master)
		self.parent = master
		self.URL = URL
		self.all = all
		self.control = control
		self.imageId = imageId	
		self.initUI()

	def initUI(self):
		if hasattr(self.parent, 'tab'):
			self.frame = Frame(self.parent.tab, bd=2, relief=SUNKEN)
		else:
			self.frame = Frame(self.parent, bd=2, relief=SUNKEN)
		
		self.frame.grid_rowconfigure(0, weight=1)
		self.frame.grid_columnconfigure(0, weight=1)

		self.canvas = ImageCanvas.ImageCanvas(self.frame,URL=self.URL, imageId=self.imageId, all=self.all)
		self.frame.pack(fill=BOTH,expand=1)
		#print "View Frame self.canvas"
		#print self.canvas
		self.addMouseEvent()
	
	def addMouseEvent(self):
		self.canvas.bind("<ButtonPress>", self.printcoords)	
		
	def printcoords(self, event):
		print "View FrameObject run"
		frameEvent = event.widget
		print frameEvent
		
	def setActive(self):
		self.frame.configure(background='red')
		
	def setInactive(self):
		self.frame.configure(background='white')
		
	
