'''
Created on Oct 1, 2015

@author: master
'''

from Tkinter import *
from PIL import Image, ImageTk
from ttk import Notebook
#import console
#import os
import shutil
from View import FrameObject
from View import ImageCanvas

class TabObject(Frame):
	
	#canvas = None
	#activeId = False
	acvite = False
	
	def __init__(self, master = None, URL = None, imageId = None, all = None, tabName = None):
		Frame.__init__(self, master)
		self.parent = master
		self.URL = URL
		self.imageId = imageId
		self.tabName = tabName
		self.all = all
		self.initUI()

	def initUI(self):
		self.pack(fill=BOTH,expand=1)
		#self.tab = Frame(self.parent)
		#self.frameObject = FrameObject.FrameObject(self,URL=self.URL, all=self.all, imageId=self.imageId)
		self.canvas = ImageCanvas.ImageCanvas(self,URL=self.URL, imageId=self.imageId, all=self.all)
		self.parent.add(self, text = self.tabName, compound=TOP)
		
		
	def getFrame(self):
		return self.canvas
