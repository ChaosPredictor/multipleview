'''
Created on Oct 4, 2015

@author: master
'''
import View.AddPointPopup

class Popup():

	def __init__(self, master=None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		self.addPoint = View.AddPointPopup.AddPointPopup(self.parent, all = self.all)
		#self.fortest()