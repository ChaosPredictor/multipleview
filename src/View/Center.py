from Tkinter import *
from PIL import Image, ImageTk
#import console
#import os
import shutil
from View import ImageCanvas
from ttk import Notebook
from tkFileDialog import askopenfilename
from View import FrameObject
from View import TabObject
from Assist.HelpFunctions import nameFromURL

class Center(Frame):

	activeImageId = None
	activePane = 0  #0-left; 1-right
	leftImageList = []
	rightImagelist = []
	leftTabs = []
	rightTabs = []
	active = None
	acviteLeft = None
	acviteRight = None
	counter = 0
	rightNote = None
	leftNote = None
	leftNotebook = None
	rgithNotebook = None
	
	def __init__(self, master=None, all=None):
		Frame.__init__(self, master)
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):		
		panes = PanedWindow(self.parent, bg="#202020", sashwidth=10)
		panes.pack(fill="both", expand="yes")

		self.left = Label(panes, width = 75)
		self.right = Label(panes)
		
		self.left.pack()
		self.right.pack()
	
		self.left.update()
		
		panes.add(self.left)
		panes.add(self.right)
		
	def active(self, act):
		self.imageList[self.activeId].act
		
	def addImage(self):
		URL = askopenfilename(parent=self.parent, initialdir="C:/",title='Choose an image to add')
		self.addTab(URL, self.activeSide)	
			
	def TabsEvent(self, event):
		self.all.get("control").tab.changeAcvite(event)

	def createImageObject(self, pane, imageId):
				
		return ImageCanvas.ImageCanvas(pane, URL=self.all.get("model").image.getURLByImageId(imageId), imageId=imageId, all = self.all)
		#return FrameObject.FrameObject(pane, URL=self.all.get("model").image.getURLByImageId(imageId), imageId=imageId, all = self.all)
	
	def destroyAllPaneChildren(self, pane):
		for frame in pane.children.values():
			frame.destroy()
			
	def createNotebook(self, widget = None):
		return Notebook(widget)
	
	def createTabObject(self, note, imageId = None, tabName = None):
		URL=self.all.get("model").image.getURLByImageId(imageId)
		fileName = nameFromURL(URL)
		tab = TabObject.TabObject(note, URL = URL, imageId = imageId,  all = self.all, tabName = fileName)
		return tab
		
	def packNote(self, note):
		note.pack(fill=BOTH,expand=1)
	