'''
Created on Sep 27, 2015

@author: master
'''

from Tkinter import *
from Tkinter import Tk, Widget
from PIL import Image, ImageTk
import Image
import ImageTk


class ImageCanvas(Frame):
	
	convas = None
	
	def __init__(self, master = None, URL=None, imageId=None,  all=None):
		Frame.__init__(self, master)
		self.parent = master
		self.URL = URL
		self.imageId = imageId
		#print "View Image imageId:"
		#print imageId
		self.all = all
		self.initUI()

	def initUI(self):
		self.pack(fill=BOTH,expand=1)
		
		self.grid_rowconfigure(0, weight=1)
		self.grid_columnconfigure(0, weight=1)

		self.zoom = 1
		self.xscroll = Scrollbar(self, orient=HORIZONTAL)
		self.xscroll.grid(row=1, column=0, sticky=E+W)
		self.yscroll = Scrollbar(self)
		self.yscroll.grid(row=0, column=1, sticky=N+S)
		self.canvas = Canvas(self, bd = 0, bg = "white", xscrollcommand=self.xscroll.set, yscrollcommand=self.yscroll.set)
		
		#print "View Image changeZoom"
		#print canvas		
		
		self.canvas.grid(row=0, column=0, sticky=N+S+E+W)
		self.xscroll.config(command=self.canvas.xview)
		self.yscroll.config(command=self.canvas.yview)
		
		
				
		if self.URL == None:
			self.URL = "../img/Stalin.jpeg"
		img = Image.open(self.URL)
		[self.originalImageSizeWidth, self.originalImageSizeHeight] = img.size
		[self.currentImageSizeWidth,self.currentImageSizeHeight] = [self.originalImageSizeWidth, self.originalImageSizeHeight]
		self.image = ImageTk.PhotoImage(img,master = self.canvas)		
		
		self.addMouseEvent(self.canvas)		
		
		self.canvas.create_image(0, 0,image = self.image)	
		self.canvas.config(scrollregion=self.canvas.bbox(ALL))
	
		
	def getImageSize(self):
		return [self.currentImageSizeWidth, self.currentImageSizeHeight]

	def changeZoom(self, zoom):
		self.zoom *= zoom
		self.currentImageSizeWidth = int(self.currentImageSizeWidth*zoom)
		self.currentImageSizeHeight = int(self.currentImageSizeHeight*zoom)
		img = Image.open(self.URL)
		img = img.resize((self.currentImageSizeWidth, self.currentImageSizeHeight), Image.ANTIALIAS)
		#self.canvas = Canvas(self.parent.frame, bd = 0, bg = "white", xscrollcommand=self.xscroll.set, yscrollcommand=self.yscroll.set)
		
		self.canvas = Canvas(self, bd = 0, bg = "white", xscrollcommand=self.xscroll.set, yscrollcommand=self.yscroll.set)
		self.canvas.grid(row=0, column=0, sticky=N+S+E+W)
		#self.canvas.grid(row=0, column=0, sticky=N+S+E+W)
		#print "View Image changeZoom"
		#print canvas
		self.image = ImageTk.PhotoImage(img, master=self.canvas)
			

		self.canvas.create_image(0, 0,image = self.image)		
		self.xscroll.config(command=self.canvas.xview)
		self.yscroll.config(command=self.canvas.yview)
		self.canvas.config(scrollregion=self.canvas.bbox(ALL))
		self.addMouseEvent(self.canvas)
	
	def addMouseEvent(self, canvas):
		#print "View ImageCanvas add run"
		self.canvas.bind("<Button-1>", self.printcoords)
		#self.canvas.bind("<Button-1>", self.printcoords)

		
	def printcoords(self, event):
		#print "View ImageCanvas run"
		canvasEvent = event.widget
		x = canvasEvent.canvasx(event.x)
		y = canvasEvent.canvasy(event.y)
		
		#print self.parent.active
		#if self.parent.active == True:
		#print "View Image event.widget"
		#print imageEvent
		#canvas2 = self.all.get("control").frame.getActiveCanvas() 
		#print canvas2
		#frame2 = self.all.get("control").frame.getActiveFrame() 
		#print frame2
		
		if (self.all.get("control").frame.getActiveCanvas() == canvasEvent):
		#if (self.all.get("model").image.getAcvite() == self.imageId):
			if (abs(x) > self.currentImageSizeWidth/2):
				#return False
				pass
			elif (abs(y) > self.currentImageSizeHeight/2):
				#return False
				pass
			else:
				#frameParentName = self.parent.parent.winfo_parent()
				#frameParent     = self.parent.parent._nametowidget(frameParentName)
				#frameParentName2= frameParent.winfo_parent()
				#frameParent2     = frameParent._nametowidget(frameParentName2)
				#frameParentName3= frameParent2.winfo_parent()
				#frameParent3     = frameParent._nametowidget(frameParentName3)
				
				imageX = (self.currentImageSizeWidth/2+x)/self.zoom
				imageY = (self.currentImageSizeHeight/2-y)/self.zoom
				if self.all.get("view").popup.addPoint.getIsOpen() == False:
					self.all.get("control").point.addPoint(self.imageId, [imageX,imageY])										
				#print frameParent3.parent
				#print ((x+self.currentImageSizeWidth/2)/self.zoom,(y+self.currentImageSizeHeight/2)/self.zoom)
				#print ((x+self.currentImageSizeWidth/2)/self.zoom,(-y+self.currentImageSizeHeight/2)/self.zoom)
		else:
			return False
		
	def setActive(self):
		self.configure(background='red')
		
	def setInactive(self):
		self.configure(background='white')
	
	def addPoint(self, location, color):
		x = location[0]*self.zoom
		y = location[1]*self.zoom
		size = 15
		xCenter = x - self.currentImageSizeWidth/2
		yCenter = -y + self.currentImageSizeHeight/2
		x0 = xCenter - size/2
		y0 = yCenter + size/2
		x1 = x0 + size
		y1 = y0 - size
		#print x0, y0, x1, y1
		object = []
		object.append(location)
		object.append(self.canvas.create_oval(x0, y0, x1, y1, outline = color))
		object.append(self.canvas.create_oval(xCenter, yCenter - 1, xCenter, yCenter - 1, outline = "red"))
		
		#print "View ImageConvas addPoint, "
		return object
	
	def reconfigPoint(self, object, color, pointNum):
		#print "View ImageConvas addPoint, canvas:", self.canvas, ", object:", object, "\n"
		self.canvas.itemconfig(object[1], outline = "red")
		#print(self.canvas.coords(object[0])) #central dot
		location = object[0]
		x = location[0]*self.zoom
		y = location[1]*self.zoom
		xCenter = x - self.currentImageSizeWidth/2
		yCenter = -y + self.currentImageSizeHeight/2
			
		object.append(self.canvas.create_text(xCenter + 12, yCenter - 12, fill = "red", text = pointNum))

	def deletePoint(self, object):
		#print "View ImageConvas deletePoint, canvas:", self.canvas, ", object:", object, "\n"
		self.canvas.delete(object[0])
		self.canvas.delete(object[1])
		
	