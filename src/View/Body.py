'''
Created on Oct 4, 2015

@author: master
'''
from View import UpperMenu
from View import Center
from Tkinter import Frame

class Body():

	def __init__(self, master=None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		self.parent.update()
		self.menu = UpperMenu.UpperMenu(self.parent, all = self.all)
		self.center = Center.Center(self.parent, all = self.all)