from Tkinter import *
from PIL import Image, ImageTk


class UpperMenu(Frame):
	
	main = None
	
	def __init__(self, master=None, all=None):
		Frame.__init__(self, master)
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		#print "UpperMenu start"
		self.menu = Menu(self.parent)
		#File Menu
		filemenu = Menu(self.menu, tearoff=0)
		filemenu.add_command(label="New", command = self.hello)
		filemenu.add_command(label="Open", command = self.hello)
		filemenu.add_command(label="Save", command = self.hello)
		filemenu.add_separator()
		filemenu.add_command(label="Exit", command=self.parent.quit)
		self.menu.add_cascade(label="File", menu=filemenu)
		#Edit Menu
		editmenu = Menu(self.menu, tearoff=0)
		editmenu.add_command(label="Switch Active", command = self.switchPane)		
		editmenu.add_separator()
		editmenu.add_command(label="Cut", command = self.callFromMain)
		editmenu.add_command(label="Copy", command = self.hello)
		editmenu.add_command(label="Paste", command = self.hello)
		editmenu.add_separator()
		editmenu.add_command(label="Select All", command = self.hello)		
		self.menu.add_cascade(label="Edit", menu=editmenu)
		#Zoom Menu
		zoommenu = Menu(self.menu, tearoff=0)
		zoommenu.add_command(label="Zoom in", command = self.activeChangeZoomIn)
		zoommenu.add_command(label="Zoom out", command = self.activeChangeZoomOut)
		self.menu.add_cascade(label="Zoom", menu=zoommenu)
		#Image Menu
		imagemenu = Menu(self.menu, tearoff=0)
		imagemenu.add_command(label="Add image", command = self.addImage)
		imagemenu.add_separator()
		imagemenu.add_command(label="Get Image list", command = self.getImageList)
		self.menu.add_cascade(label="Image", menu=imagemenu)		
		#Point Menu
		pointmenu = Menu(self.menu, tearoff=0)
		pointmenu.add_command(label="Get Point List", command = self.getPointList)
		pointmenu.add_command(label="Save to File", command = self.savePointToFiel)			
		pointmenu.add_command(label="Save to JSON", command = self.savePointToJSON)			
		pointmenu.add_command(label="Get Point List of Active Image", command = self.getPointListOfAcvite)	
		pointmenu.add_command(label="claer DB", command = self.clearPointsDB)	
		self.menu.add_cascade(label="Point", menu=pointmenu)
		#Analysis
		analysismenu = Menu(self.menu, tearoff=0)
		analysismenu.add_command(label="Load from File", command = self.analysisLoad)
		analysismenu.add_command(label="Load from DB", command = self.analysisLoadFromDB)		
		self.menu.add_cascade(label="Analysis", menu=analysismenu)
		#Test options
		testmenu = Menu(self.menu, tearoff=0)
		testmenu.add_command(label="Test1", command = self.test1)
		self.menu.add_cascade(label="Test", menu=testmenu)		
		
		self.parent.config(menu=self.menu)
		

		
	def addImage(self):
		self.all.get("control").image.addImage()
		
	def callFromMain(self):
		self.all.get("view").main.fromTest()   #TODO move to control
		
	def activeChangeZoomIn(self):
		self.all.get("control").frame.getActiveFrame().changeZoom(2)

	def activeChangeZoomOut(self):
		self.all.get("control").frame.getActiveFrame().changeZoom(0.5)		

	def switchPane(self):
		self.all.get("control").pane.switchPane()
		
	def getPointList(self):
		self.all.get("control").point.getPointList()

	def getImageList(self):
		self.all.get("control").image.getImageList()
		
	def getPointListOfAcvite(self):
		self.all.get("control").point.addToCanvasPointListByImageId(0)

	def savePointToFiel(self):
		self.all.get("control").point.savePointToFiel()
		
	def savePointToJSON(self):
		self.all.get("control").point.savePointToJSON()
		
	def clearPointsDB(self):
		self.all.get("control").point.clearDB()
		
	def analysisLoad(self):  #not in use
		self.all.get("analysis").loadFile("555")

	def analysisLoadJSON(self):
		self.all.get("analysis").loadJSON("555")

	def analysisLoadFromDB(self):
		self.all.get("analysis").loadPointsFromDB()
				
	def test1(self):
		activeImage = self.all.get("control").frame.getActive()
		print activeImage
		activeImage.addPoint([100,150], "red")
		
	def hello(self):
		print "View UpperMenu - not done"
	
