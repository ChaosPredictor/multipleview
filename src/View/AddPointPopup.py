'''
Created on Oct 3, 2015

@author: master
'''

from Tkinter import *
from Assist.Table import Table
import Tkinter as tk


class AddPointPopup(Frame):

	isOpen = False
	toplevel = None

	def __init__(self, master=None, all=None):
		Frame.__init__(self, master)
		self.all = all
		self.initUI()

	def initUI(self):
		pass
		
	def Open(self, imageId, location, viewObject):
		self.setIsOpen(True)

		self.imageId = imageId
		self.location = location
		self.viewObject = viewObject
		
		self.toplevel = Toplevel()
		self.toplevel.geometry("%dx%d+%d+%d" % (250, 200, 200, 200))
		self.toplevel.wm_title("Add new point")
		label = Label(self.toplevel, height=1, width=50)
		label.pack()
		
		table = Table(self.toplevel,2,2)
		table.pack(side="top", fill="x")
		table.set(0,0,"Add point X/Y")
		locationText = str(self.location[0]) + "/" + str(self.location[1])
		table.set(1,0, locationText)
		
		table.set(0,1,"To image")
		table.set(1,1,str(self.imageId))	

		label2 = Label(self.toplevel, height=3, width=50)
		label2.pack()
		
		label = Label(label2, text="As Point Number", height=3, width=15)
		label.pack(side=tk.LEFT)	
		
		self.text = Entry(label2, width=10)
		self.text.pack(side=tk.LEFT)
		self.text.insert(0, "1")
				
		label3 = Label(self.toplevel, height=3, width=50)
		label3.pack()
		
		close_buttonOk = tk.Button(label3, text="OK", command=self.OkButtonPressed)
		close_buttonClose = tk.Button(label3, text="Cancel", command=self.CancelButtonPressed)
		close_buttonOk.pack(side=tk.LEFT)
		close_buttonClose.pack(side=tk.LEFT)
		
	def selectAll(self):
		self.text.selection_range(0, END)
				
	def OkButtonPressed(self):
		#self.pointNumber = self.text.get("1.0",END)
		self.pointNumber = self.text.get()
		self.all.get("control").point.addPointOk(self.imageId, self.location, self.pointNumber, self.viewObject)										
		self.setIsOpen(False)
		self.toplevel.destroy()
	
	def CancelButtonPressed(self):
		self.all.get("control").point.deletePoint(self.viewObject)
		self.setIsOpen(False)
		self.toplevel.destroy()
		
	def getIsOpen(self):
		return self.isOpen
	
	def setIsOpen(self, status):
		self.isOpen = status
		