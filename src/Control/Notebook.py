'''
Created on Oct 12, 2015

@author: master
'''
from zeitgeist.datamodel import parents

class Notebook():
	
	leftNotebook = None
	rightNotebook = None
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def createNotebook(self, parent, side):
		notebook = self.all.get("view").body.center.createNotebook(widget = parent)
		notebook.bind("<<NotebookTabChanged>>", self.all.get("control").tab.changeActive)
		self.all.get("view").body.center.packNote(notebook)
		#print "Control Notebook createNotebook, side:", side, " ,Notebook:", notebook, "\n"
		self.setNotebookBySide(side, notebook)
		#print "Control Notebook addNotebook run"
		return notebook
	
	def setFocus(self, notebook, tab = None):
		side = self.getSideByNotebook(notebook)
		#print "Control Notebook setFocus, side:", side, ", notebook:", notebook, "\n"
		if tab == None:
			tab = self.all.get("control").tab.getTopBySide(side)
		notebook.select(tab)
		#print "Control NoteBook setFocus start", "\n"		
		self.all.get("control").tab.setTopBySideActive(side, tab)
		#print "Control NoteBook setFocus end", "\n"
	
	def getSideByNotebook(self, notebook):
		#print "Control Notebook getSideByNotebook, leftNotebook:", self.leftNotebook, ", rightNotebook:", self.rightNotebook, "\n"
		if notebook == self.leftNotebook:
			side = "left"
		elif notebook == self.rightNotebook:
			side = "right"
		return side		
		
	def getNotebookBySide(self, side):
		if side == "left":
			return self.leftNotebook
		elif side == "right":
			return self.rightNotebook
		
	def setNotebookBySide(self, side, notebook):
		if side == "left":
			self.leftNotebook = notebook
			return True
		elif side == "right":
			self.rightNotebook = notebook
			return True
		return False
		