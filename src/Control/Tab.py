'''
Created on Oct 6, 2015

@author: master
'''
from Control import Notebook

class Tab():
	
	topLeft = None
	topRight = None
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def addTab(self, imageId, side, notebook = None):
		if notebook == None:
			notebook = self.all.get("control").notebook.getNotebookBySide(side)

		tab = self.all.get("view").body.center.createTabObject(notebook, imageId = imageId, tabName = None)
		#print "Control Tab addTab, Tab:", tab, ", imgeId:", imageId, "\n"
		return tab
	
	def addTabTop(self, imageId, side, notebook = None):
		if notebook == None:
			notebook = self.all.get("control").notebook.getNotebookBySide(side)
		#print "Control Tab addTabTop, notebook:", notebook, ", imageId:", imageId, ", side:", side, "\n"
		tab = self.addTab(imageId = imageId, side = side)
		self.setTopBySide(side, tab)
		return tab

	
	def changeActive(self, event):
		#print "Control Tab changeActive, start" , "\n"
		oldAcviteFrame = self.all.get("control").frame.getActive()

		leftNotebook = self.all.get("control").notebook.getNotebookBySide("left")
		rightNotebook = self.all.get("control").notebook.getNotebookBySide("right")

		change = False
		#print "Control Tab changeActive, widget:", event , "\n"
		#print event.widget.index("current") ,"\n"
		
		if event.widget == leftNotebook:
			activeTab = leftNotebook.winfo_children()[event.widget.index("current")]
			self.setTopBySide(side = "left", tab = activeTab)			
			if self.all.get("control").pane.getChosen() == "left":
				change = True
				#self.all.get("model").pane.active = "left"  #TODO move to control
			if self.all.get("control").pane.getChosen() == "right":
				pass
		elif event.widget == rightNotebook:
			activeTab = rightNotebook.winfo_children()[event.widget.index("current")]
			self.setTopBySide(side = "right", tab = activeTab)
			#print "Control Tab right", "\n"
			if self.all.get("control").pane.getChosen() == "right":
				change = True
				#self.all.get("model").pane.active = "right" #TODO move to control, change to chosen
			if self.all.get("control").pane.getChosen() == "left":
				pass
			
		#print "Control Tab changeActive, side:" , event.widget, "\n"
		
		if change == True:
			self.all.get("control").frame.setFrameInacvite(oldAcviteFrame)
			frame = activeTab.getFrame()
		
			self.all.get("control").frame.setFrameActive(frame)
			#self.all.get("control").pane.setChosen(self.all.get("model").pane.active)
	
		#print "Control Tab setActive"
		#print frame
	
	def getTopBySide(self, side):
		#print "Control Tab getTopbySide side:", "\n", side, "\n"
		if side == "right":
			return self.topRight
		elif side == "left":
			return self.topLeft
		
	def setTopBySideActive(self, side, tab):
		#print "Control Tab setTopBySideActive, side:", side, ", Tab:", tab, "\n"
		self.setTopBySide(side, tab)

		frame = tab.getFrame()

		self.all.get("control").frame.setFrameActive(frame)

	def setTopBySide(self, side, tab):
		#print "Control Tab setTopBySide, side:", side, ", Tab:", tab, "\n"
		if side == "right":
			self.topRight = tab
		elif side == "left":
			self.topLeft = tab

	def setTop(self, tab):
		frame = tab.getFrame()
		self.all.get("control").frame.setFrameActive(frame)