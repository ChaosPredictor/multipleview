'''
Created on Oct 6, 2015

@author: master
'''

from tkFileDialog import askopenfilename

class Image():
	
	#TODO move imageId counter here
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def addImage(self, URL=None, side=None):
		if URL == None:
			URL = askopenfilename(parent=self.parent, initialdir="../img/",title='Choose an image to add')
		if URL == "":
			return None
		if side == None:
			side = self.all.get("control").pane.getChosen()
		imageId = self.all.get("model").image.addImage(URL, side)
		pane = self.all.get("control").pane.returnPaneBySide(side)
		counter = self.all.get("model").tab.returnCounterBySide(side)
		#print "Control Image AddImage:", counter, " side:",side,"\n"

		if counter == 0:
			self.all.get("control").frame.addFrame(pane, imageId)
		
		elif counter == 1:
			self.all.get("control").pane.destroyFrameChildren(pane)
			
			self.all.get("control").notebook.createNotebook(pane, side)

			image0 = self.all.get("model").image.getOnlyImageIdbySide(side)
			self.all.get("control").tab.addTab(imageId = image0, side = side)
			self.all.get("control").tab.addTabTop(imageId = imageId, side = side)
			
		elif counter > 1:
			self.all.get("control").tab.addTabTop(imageId = imageId, side = side)
			
		self.all.get("control").pane.setActivePaneBySide(side, pane)
		self.all.get("model").tab.setCounterBySide(side,counter+1)
		
	def getImageList(self):
		imageIdList = self.all.get("model").image.getImagesIdList()
		for imageId in imageIdList:
			print self.all.get("model").image.getImageByImageId(imageId)	