'''
Created on Oct 6, 2015

@author: master
'''

import json

class Point():
	
	pointId = 0
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def addPoint(self, imageId, location):
		self.currentActiveImage = self.all.get("control").frame.getActive()
		viewObject = self.currentActiveImage.addPoint(location, "green")
		self.all.get("view").popup.addPoint.Open(imageId, location, viewObject)
		
	def addPointOk(self, imageId, location, pointNum, viewObject):
		try:
			intPointNum = int(pointNum)  # @IndentOk
			if (intPointNum > 0) & (intPointNum < 100):
				self.all.get("model").point.addPoint(imageId, location, intPointNum, self.pointId, viewObject)
				self.all.get("model").point.addPointToDB(imageId, location, intPointNum, self.pointId, viewObject)
				self.currentActiveImage.reconfigPoint(viewObject, "red", pointNum)
				self.pointId += 1
			else:
				print "Control Point addpointOk illegal point number - out of border"
		except ValueError:
			print "Control Point addpointOk illegal point number - can't int"
		self.currentActiveImage = None
		
	def deletePoint(self, viewObject):
		self.currentActiveImage.deletePoint(viewObject)
		self.currentActiveImage = None
	
	def getPointsIdList(self):
		self.all.get("model").point.getPointsIdList()
		
	def getPointList(self):
		pointIdList = self.all.get("model").point.getPointsIdList()
		for pointId in pointIdList:
			print self.all.get("model").point.getPointByPointId(pointId)
			
	def addToCanvasPointListByImageId(self, imageId):
		activeImage = self.all.get("control").frame.getActive()
		points =  self.all.get("model").point.getPointListByImageId(imageId)
		for point in points:
			activeImage.addPoint(point[0], "red")
		
	def getPointListOfActive(self):    ### still NOT working 
		activeImage = self.all.get("control").frame.getActive()
		
	def savePointToFiel(self):
		fileToSave = open('../output/points.data', 'w')
		pointIdList = self.all.get("model").point.getPointsIdList()
		for pointId in pointIdList:
			pointString = str(self.all.get("model").point.getPointByPointId(pointId))
			pointString += "\n"
			fileToSave.write(pointString)
		fileToSave.close()	
		
	def savePointToJSON(self):   #not working TODO
		with open('../output/points.json', 'w') as outfile:
			pointIdList = self.all.get("model").point.getPointsIdList()
			data = None
			string = "["
			for pointId in pointIdList:
				point = self.all.get("model").point.getPointByPointId(pointId)
				location = point[0]
				imageId = point[1]
				pointNum = point[2]
				pointId = point[3]
				string += str({"location" : location, "imageId" : imageId})
				string += ","
			slist = list(string)	
			slist[-1] = "]"
			string = "".join(slist)
				#data = json.dumps({"location" : location, "imageId" : imageId})
			data = json.dumps(string)			#for pointId in pointIdList:
			json.dump(data, outfile)			
		
	def clearDB(self):
		self.all.get("model").point.clearDB()