'''
Created on Oct 7, 2015

@author: master
'''
from zeitgeist.datamodel import parents

class Frame():
	
	active = None
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def getFrame(self, frame):
		return frame
		#return frame.canvas    # TODO change to image
	
	def getActiveFrame(self):
		#print "Control Frame activeImage"
		#print self.getImage(self.active)
		return self.getFrame(self.active)
	
	def getCanvas(self, frame):
		return frame.canvas
		#return frame.canvas    # TODO change to image
	
	def getActiveCanvas(self):
		#print "Control Frame activeImage"
		#print self.getImage(self.active)
		return self.getCanvas(self.active)
	
	
	def addFrame(self, parent, imageId):
		frame = self.all.get("view").body.center.createImageObject(parent, imageId)
		self.all.get("model").frame.addFrame(frame, imageId)
		#print "Control Frame addFrame, imageId:", imageId,", frame:", frame, "\n"
		return frame
	
	def addFrameActive(self, parent, imageId):
		frame = self.addFrame(parent, imageId)
		self.setFrameActive(frame)
		return frame
	
	
	def setFrameActive(self, frame):
		#print "Control Frame setFrameActive, frame:", frame, "\n"
		#print "Control Frame setFrameActive", "\n", frame, "\n"
		if self.active != None:
			self.setInacvite()
		self.active = frame		
		#self.all.get("model").frame.setActive(frame)
		frame.setActive()
							
	def setFrameInacvite(self, frame):
		#print "Control Frame setFrameInacvite run"
		#print self.active
		frame.setInactive()
		
	def setInacvite(self):
		#print "Control Frame setInactive"
		#print self.active		
		self.setFrameInacvite(self.active)
	
	def setAcviteNone(self):
		self.active = None
	
	def getActive(self):
		#print "Control Frame getActive run"
		return self.active
	
	def getActiveImageId(self):
		pass
	
	