'''
Created on Oct 6, 2015

@author: master
'''

class Pane():
	
	chosen = "left"
	leftNote = None
	rightNote = None
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
	
	def setChosen(self, side):
		#print "Control Pane", side, "\n"
		self.chosen = side
		
	def setActiveChosen(self, side, pane):
		self.setChosen(side)
		child = pane.children.values()[0]
		childName = child.__class__.__name__
		#print "Control Pane setActiveChosen, Pane:", pane, ", side:", side, "\n"
		
		if childName == "ImageCanvas":
			frame = child
			self.all.get("control").frame.setFrameActive(frame)
		elif childName == "Notebook":
			notebook = child
			self.all.get("control").notebook.setFocus(notebook)
	
	def getChosen(self):
		return self.chosen
	
	def switchPane(self):
		oldSide = self.getChosen()

		if oldSide == "right":
			newSide = "left"
		elif oldSide == "left":
			newSide = "right"			
			
		#print "Control Pane switchPane run", newSide, "\n"		
		self.changeChosenPane(newSide)
		
	def changeChosenPane(self, side):
		#maybe TODO change of counter
		self.setChosen(side) 
		counter = self.all.get("model").tab.returnCounterBySide(side)
		frame = None
		if counter == 1: # Frame
			pane = self.returnPaneBySide(side)
			frame = self.getPaneChild(pane)
		elif counter > 1: # Notebook
			topTab = self.all.get("control").tab.getTopBySide(side)
			frame = topTab.getFrame()
		
		self.all.get("control").frame.setInacvite()
		if frame != None:
			self.all.get("control").frame.setFrameActive(frame)
				
	def getPaneChild(self, pane):
		#print "Control Pane paneChild"
		#print pane.winfo_children()[0]
		return pane.winfo_children()[0]
	
	def returnPaneBySide(self, side):
		pane = None
		if side == "left":
			pane = self.all.get("view").body.center.left    #TODO move to model
		elif side == "right":
			pane = self.all.get("view").body.center.right    #TODO move to model
		else:
			print "illegal pane"
		return pane
		
	def setPaneBySide(self, side, pane):
		if side == "left":
			self.all.get("view").body.center.left = pane   #TODO move to model
		elif side == "right":
			self.all.get("view").body.center.right = pane    #TODO move to model
		else:
			print "illegal pane"
	
	def setActivePaneBySide(self, side, pane):
		#print "Control Pane setActivePaneBySide side:", side, ", pane:" , pane,  "\n"
		self.setPaneBySide(side, pane)
		self.setActiveChosen(side, pane)

	def returnNotebookBySide(self, side):
		return self.all.get("control").tab.getNotebookBySide(side)
	
	def destroyAllPaneChildren(self, pane):
		#print "Control Pane destroyAllPaneChildren, Pane:", pane, "\n"
		for frame in pane.children.values():
			frame.destroy()
		self.all.get("control").frame.setAcviteNone()
			
	def destroyFrameChildren(self, pane):
		#print "Control Pane destroyFrameChildren, Pane:", pane, "\n"
		frame = pane.children.values()[0]
		image = frame.children.values()
		frame.destroy()
		self.all.get("control").frame.setAcviteNone()
		return image