'''
Created on Oct 5, 2015

@author: master
'''
from Input import Keyboard
from Input import Mouse

class Input():
	
	def __init__(self, master = None):
		self.parent = master
		self.initUI()

	def initUI(self):
		pass
		
	def getOther(self, all=None):
		self.all = all
		
	def update(self):
		self.keyboard = Keyboard.Keyboard(self.all)		
		self.mouse = Mouse.Mouse(self.all)
		
	def run(self):
		pass