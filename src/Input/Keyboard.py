'''
Created on Oct 5, 2015

@author: master
'''

class Keyboard():
	
	def __init__(self, all=None):
		self.all = all
		self.initUI()

	def initUI(self):
		self.all.get("view").root.bind("<Key>", self.fortest)
		self.all.get("view").root.bind("<Control-q>", self.exit)
		self.all.get("view").root.bind("<Control-s>", self.switchPane)
		
	
	def setMain(self, main):
		self.main = main
	
	def switchPane(self, event):
		self.all.get("control").pane.switchPane()
		
	def exit(self, event):
		self.all.get("view").root.destroy()
		#if event.char == '\x11':
			#self.all.get("view").root.destroy()
			
	def fortest(self, event):
		pass