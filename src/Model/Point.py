'''
Created on Oct 4, 2015

@author: master
'''

from collections import defaultdict



class Point():
	
	
	
	pointList = defaultdict(list)
	
	def __init__(self, db = None, all=None):
		self.collection = db.point
		self.all = all
		self.initUI()

	def initUI(self):
		pass
		#self.fortest()
	
	def addPoint(self, imageId, location, pointNum, pointId, viewObject):
		#print "Model Point addPoint run"
		point = [location , imageId, pointNum, pointId, viewObject]
		self.pointList[pointId].append(point)

	def addPointToDB(self, imageId, location, pointNum, pointId, viewObject):
		point = {
				"_id" : pointId, 
				"imageId" : imageId,
				"location" : location,
				"pointNum" : pointNum
				}
		self.collection.insert(point)
					
	def clearDB(self):
		self.collection.remove()
		
	def getAllPoints(self):
		pass
	
	def getPointsIdList(self):
		listOfImageId = []
		for pointId in self.pointList:
			listOfImageId.append(pointId)
		return listOfImageId

	def getPointListByImageId(self, imageId):
		listOfImage = []
		for pointId in self.pointList:
			if self.getImageIdFromPoint(self.pointList[pointId][0]) == imageId:
				listOfImage.append(self.pointList[pointId][0])
		return listOfImage
	
	def getImageIdFromPoint(self, point):
		return point[1]

	def getPointByPointId(self, pointId):
		return self.pointList[pointId][0]
		
	def checkIfImageHavePointNum(self):  #TODO
		pass 
	
	def getLinkCollection(self):
		return self.collection