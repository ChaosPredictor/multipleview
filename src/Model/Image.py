'''
Created on Oct 6, 2015

@author: master
'''

from collections import defaultdict


class Image():
	
	imageId = 0
	active = None

	imageList = defaultdict(list)
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
		#self.fortest()
		
	def addImage(self, URL, side=None):
		#print "Model Image URL,side:"
		#print URL, side
		self.active = self.imageId
		if side == None:
			side = self.all.get("model").pane.getActive()
		image = [URL,side,self.imageId]
		self.imageList[self.imageId].append(image)
		self.imageId += 1
		return self.imageId - 1
		print "TODO move DB:"
		print URL
		
	def avtiveChanged(self, imageId):
		self.active = imageId
		
	def getActive(self):
		return self.active
		
	def getImageByImageId(self, imageId):
		return self.imageList[imageId][0]

	def getURLByImageId(self, imageId):
		#print "Model Image imageID:"
		#print imageId
		return self.imageList[imageId][0][0]
	
	def getSideByImageId(self, imageId):
		return self.imageList[imageId][0][1]
	
	def getSideFromImage(self, image):
		return image[1]
	
	def getOnlyImageIdbySide(self, side):
		for imageId in self.imageList:
			if self.getSideFromImage(self.imageList[imageId][0]) == side:
				return imageId

	def getImagesIdList(self):
		listOfImageId = []
		for imageId in self.imageList:
			listOfImageId.append(imageId)
		return listOfImageId	
	