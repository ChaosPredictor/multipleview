'''
Created on Oct 6, 2015

@author: master
'''
from View import FrameObject

class Tab():
	
	tabId = 0
	rightNotebook = None
	leftNotebook = None
	rightAcvite = None
	leftActive = None
	rightTop = None
	leftTop = None
	rightCounter = 0
	leftCounter = 0
	active = None
	
	def __init__(self, master = None, all=None):
		self.parent = master
		self.all = all
		self.initUI()

	def initUI(self):
		pass
		
	#def activeChanged(self, imageId):
	#	self.active = imageId
		
	def getAcvite(self):
		return self.active
		
	def returnCounterBySide(self, side):
		counter = None
		if side == "left":
			counter = self.all.get("model").tab.leftCounter
		elif side == "right":
			counter = self.all.get("model").tab.rightCounter
		else:
			print "illegal pane"
		return counter
	
	def setCounterBySide(self, side, counter):
		if side == "left":
			self.all.get("model").tab.leftCounter = counter
		elif side == "right":
			self.all.get("model").tab.rightCounter = counter
		else:
			print "illegal pane"
