'''
Created on Oct 4, 2015

@author: master
'''
import Point
import Image
import Tab
import Pane
import Frame
from pymongo import MongoClient

class Model():
	
	def __init__(self, master = None):
		self.parent = master

		self.initUI()

	def initUI(self):
		client = MongoClient()
		self.db = client.project3
		
	def getOther(self, all=None):
		self.all = all
		
	def update(self):
		self.point = Point.Point(self.db, self.all)
		self.image = Image.Image(self.parent, self.all)
		self.tab = Tab.Tab(self.parent, self.all)
		self.pane = Pane.Pane(self.parent, self.all)
		self.frame = Frame.Frame(self.parent, self.all)
		#point.fortest()
		
	def run(self):
		pass